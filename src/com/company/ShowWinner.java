package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by slobodyanyk on 20.02.16.
 */
public class ShowWinner extends Main{
    public void showWin (String winner) {
        final JFrame winWindow = new JFrame("Winner");
        winWindow.setSize(500,250);
        winWindow.setResizable(false);
        winWindow.setLocationRelativeTo(null);
        winWindow.setLayout(null);

        JButton winWindowNewGame = new JButton ("New game");
        winWindow.add(winWindowNewGame);
        winWindowNewGame.setBounds(40,115,150,100);



        if(winner.equals("Player")){
            JLabel labelWin = new JLabel("Ви");
            winWindow.add(labelWin);
            JLabel labelWin2 = new JLabel("перемогли!");
            winWindow.add(labelWin2);

            Font font = new Font("Verdana", Font.BOLD, 30);
            labelWin.setFont(font);

            labelWin.setBounds(90,20,250,50);
            labelWin2.setFont(font);
            labelWin2.setBounds(20,60,250,50);

            Icon winIcon = new  ImageIcon("Icons/happy_cat.jpg");
            JLabel icon = new JLabel(winIcon);
            icon.setBounds(250,20,200,200);
            winWindow.add(icon);


        }
        else {
            JLabel labelWin = new JLabel("Ви");
            winWindow.add(labelWin);
            JLabel labelWin2 = new JLabel("програли...");
            winWindow.add(labelWin2);

            Font font = new Font("Verdana", Font.BOLD, 30);
            labelWin.setFont(font);

            labelWin.setBounds(90,20,250,50);
            labelWin2.setFont(font);
            labelWin2.setBounds(20,60,250,50);

            Icon winIcon = new  ImageIcon("Icons/sad_cat.jpg");
            JLabel icon = new JLabel(winIcon);
            icon.setBounds(250,20,200,200);
            winWindow.add(icon);
        }

        winWindowNewGame.addActionListener(new ActionListener( ) {
            public void actionPerformed(ActionEvent e) {
                newGame();
                useChoice();
                winWindow.dispose();
            }
        });

        winWindow.setVisible(true);
    }
}
