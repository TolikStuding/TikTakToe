package com.company;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Main {
    static JFrame frame = new JFrame("Tic Tac Toe");                            // Вікно
    static String winner;
    static int nextXid;
    static int next;
    static ImageIcon iconCross = new ImageIcon("Icons/cross.jpeg");
    static ImageIcon iconZero = new ImageIcon("Icons/zero.jpeg");

    static Icon icon = null;
    static Icon iconcomp = null;
    static JButton button1=new JButton();

    static JButton button2=new JButton();

    static JButton button3=new JButton();

    static JButton button4=new JButton();

    static JButton button5=new JButton();
;
    static JButton button6=new JButton();

    static JButton button7=new JButton();
;
    static JButton button8=new JButton();

    static JButton button9=new JButton();

    static int[] b= new int[10];                                  // Допоміжний масив, кортий дозволяє програмі знати яка з позицій зайнята (тобто яка з кнопок нажата)

    public static void useChoice(){                                        // МЕТОД ВИБОРУ
        final JFrame frameChoice = new JFrame("Choice");
        frameChoice.setSize(275, 175);
        frameChoice.setResizable(false);
        frameChoice.setLocationRelativeTo(null);
        frameChoice.setLayout(null);

        for (int i=1;i<10;i++){                                           // задання допоміжному масиву потрібного значення
            b[i]=1;
        }

        JLabel labelChoice = new JLabel("Виберіть, якими фігурами");
        JLabel labelChoice2= new JLabel("будете грати:");

        Font font = new Font("Verdana", Font.BOLD, 15);
        labelChoice.setFont(font);
        labelChoice.setBounds(20,10,300,20);
        labelChoice2.setFont(font);
        labelChoice2.setBounds(20,30,300,20);
        frameChoice.add(labelChoice);
        frameChoice.add(labelChoice2);

        final ImageIcon iconCross = new ImageIcon("Icons/cross.jpeg");
        JButton buttonCross=new JButton(iconCross);
        buttonCross.setBounds(20,65,90,90);
        frameChoice.add(buttonCross);




        final ImageIcon iconZero = new ImageIcon("Icons/zero.jpeg");
        JButton buttonZero=new JButton(iconZero);
        buttonZero.setBounds(155,65,90,90);
        frameChoice.add(buttonZero);


        buttonCross.addActionListener(new ActionListener( ) {
            public void actionPerformed(ActionEvent e) {
                try {
                    game();
                    player(1);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                frameChoice.dispose();
            }
        });

        buttonZero.addActionListener(new ActionListener( ) {
            public void actionPerformed(ActionEvent e) {
                try {
                    game();
                    player(0);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                frameChoice.dispose();
            }
        });

        frameChoice.setVisible(true);
    }                                                                  // КІНЕЦЬ МЕТОДУ ВИБОРУ

    public static void game() throws IOException {                    // МЕТОД ПАНЕЛІ ГРИ
        frame.setSize(300, 331);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setBackground(Color.GREEN);

        String soundFile ="Music/okean_elzi_-_na_nebi.wav";                   // Додається музика з папки Music
        InputStream in = new FileInputStream(soundFile);
        final AudioStream audioStream = new AudioStream(in);



        final Icon iconSoundon;                                  // Клавіша музики
        final Icon iconSoundoff;
        iconSoundon = new ImageIcon("Icons/onsound3.jpeg");         // Додаються іконки із папки Icons
        iconSoundoff = new ImageIcon("Icons/offsound.jpeg");
        final Icon[] iconchange = {iconSoundoff};
        final JButton ButtonSound = new JButton(iconchange[0]);
        ButtonSound.setBounds(270,5,25,20);
        frame.add(ButtonSound);
        ButtonSound.addActionListener(new ActionListener( ) {
            public void actionPerformed(ActionEvent e) {
                if(iconchange[0] ==iconSoundoff) {
                    ButtonSound.setIcon(iconSoundon);
                    iconchange[0] = iconSoundon;
                    AudioPlayer.player.start(audioStream);       // Музика вкл і зміна іконки


                }
                else {
                    ButtonSound.setIcon(iconSoundoff);
                    iconchange[0] = iconSoundoff;
                    AudioPlayer.player.stop(audioStream);         // Музика викл і зміна іконки
                }
            }
        });


        button1.setBounds(4,35,94,94);                          // Кнопки
        frame.add(button1);

        button2.setBounds(103,35,94,94);
        frame.add(button2);

        button3.setBounds(202,35,94,94);
        frame.add(button3);

        button4.setBounds(4,134,94,94);
        frame.add(button4);

        button5.setBounds(103,134,94,94);
        frame.add(button5);

        button6.setBounds(202,134,94,94);
        frame.add(button6);

        button7.setBounds(4,233,94,94);
        frame.add(button7);

        button8.setBounds(103,233,94,94);
        frame.add(button8);

        button9.setBounds(202,233,94,94);
        frame.add(button9);

        JButton ButtonNewGame = new JButton("New game");                 //Клавіша "Нова гра"
        frame.add(ButtonNewGame);
        ButtonNewGame.setBounds(5,5,100,25);
        Font font = new Font("Verdana", Font.BOLD, 10);
        ButtonNewGame.setFont(font);
        ButtonNewGame.addActionListener(new ActionListener( ) {
            public void actionPerformed(ActionEvent e) {
                newGame();
                useChoice();
            }
        });
        frame.setVisible(true);
    }                                                                   // КІНЕЦЬ МЕТОДУ ПАНЕЛІ ГРИ

    public static void newGame () {                                    // допоміжний метод клавіші New game
        button1.setIcon(null);
        button2.setIcon(null);
        button3.setIcon(null);
        button4.setIcon(null);
        button5.setIcon(null);
        button6.setIcon(null);
        button7.setIcon(null);
        button8.setIcon(null);
        button9.setIcon(null);
        b[1]=1;
        b[2]=1;
        b[3]=1;
        b[4]=1;
        b[5]=1;
        b[6]=1;
        b[7]=1;
        b[8]=1;
        b[9]=1;
        frame.dispose();
    }

public static int searchWinnerFirstLvL (Icon iconcomp, Icon icon) {              // Метод який перевіряє чи якась з фігур займає переможні позиції і передає інфу на searchWinnerSecondsLvL
    if (b[1]==0 && b[2]==0 && b[3]==0){
        nextXid = searchWinnerSecondsLvL(iconcomp,icon);
        return nextXid;
    }
    //----------------------------------------------------
    else if (b[4]==0 && b[5]==0 && b[6]==0) {
        nextXid = searchWinnerSecondsLvL(iconcomp,icon);
        return nextXid;
    }
    //----------------------------------------------------
    else if (b[7]==0 && b[8]==0 && b[9]==0) {
        nextXid = searchWinnerSecondsLvL(iconcomp,icon);
        return nextXid;
    }
    //----------------------------------------------------
    else if (b[1]==0 && b[4]==0 && b[7]==0){
        nextXid = searchWinnerSecondsLvL(iconcomp,icon);
        return nextXid;
    }
    //----------------------------------------------------
    else if  (b[3]==0 && b[6]==0 && b[9]==0){
        nextXid = searchWinnerSecondsLvL(iconcomp,icon);
        return nextXid;
    }
    //----------------------------------------------------
    else if (b[2]==0 && b[5]==0 && b[8]==0){
        nextXid = searchWinnerSecondsLvL(iconcomp,icon);
        return nextXid;
    }
    //----------------------------------------------------
    else if (b[1]==0  && b[5]==0 && b[9]==0){
        nextXid = searchWinnerSecondsLvL(iconcomp,icon);
        return nextXid;
    }
    //----------------------------------------------------
    else if (b[3]==0 && b[5]==0 && b[7]==0 ){
        nextXid = searchWinnerSecondsLvL(iconcomp,icon);
        return nextXid;
    }
    return 0;}

    public static void player(int playersIcon) {                        // МЕТОД ХОДУ КОРИСТУВАЧА

        if(playersIcon==1) {
            icon = iconCross;
            iconcomp = iconZero;
        }
        else {
            icon = iconZero;
            iconcomp = iconCross;
        }


        button1.addActionListener(new ActionListener( ) {
            public void actionPerformed(ActionEvent e) {
                if(b[1]==1) {
                    button1.setIcon(icon);
                    b[1]=0;
                    next = searchWinnerFirstLvL(iconcomp,icon);
                    if(next==0) {
                        computer(iconcomp, icon);
                        searchWinnerFirstLvL(iconcomp, icon);
                    }
                }
            }
        });
        button2.addActionListener(new ActionListener( ) {
            public void actionPerformed(ActionEvent e) {
                if(b[2]==1) {
                    button2.setIcon(icon);
                    b[2]=0;
                    next = searchWinnerFirstLvL(iconcomp,icon);
                    if(next==0) {
                        computer(iconcomp, icon);
                        searchWinnerFirstLvL(iconcomp, icon);
                    }
                }
            }
        });
        button3.addActionListener(new ActionListener( ) {
            public void actionPerformed(ActionEvent e) {

                if(b[3]==1) {
                    button3.setIcon(icon);
                    b[3]=0;
                    next = searchWinnerFirstLvL(iconcomp,icon);
                    if(next==0) {
                        computer(iconcomp, icon);
                        searchWinnerFirstLvL(iconcomp, icon);
                    }
                }

            }
        });
        button4.addActionListener(new ActionListener( ) {
            public void actionPerformed(ActionEvent e) {

                if(b[4]==1) {
                    button4.setIcon(icon);
                    b[4]=0;
                    next = searchWinnerFirstLvL(iconcomp,icon);
                    if(next==0) {
                        computer(iconcomp, icon);
                        searchWinnerFirstLvL(iconcomp, icon);
                    }
                }
            }
        });
        button5.addActionListener(new ActionListener( ) {
            public void actionPerformed(ActionEvent e) {
                if(b[5]==1) {
                    button5.setIcon(icon);
                    b[5]=0;
                    next = searchWinnerFirstLvL(iconcomp,icon);
                    if(next==0) {
                        computer(iconcomp, icon);
                        searchWinnerFirstLvL(iconcomp, icon);
                    }
                }

            }
        });
        button6.addActionListener(new ActionListener( ) {
            public void actionPerformed(ActionEvent e) {

                if(b[6]==1) {
                    button6.setIcon(icon);
                    b[6]=0;
                    next = searchWinnerFirstLvL(iconcomp,icon);
                    if(next==0) {
                        computer(iconcomp, icon);
                        searchWinnerFirstLvL(iconcomp, icon);
                    }
                }

            }
        });
        button7.addActionListener(new ActionListener( ) {
            public void actionPerformed(ActionEvent e) {

                if(b[7]==1) {
                    button7.setIcon(icon);
                    b[7]=0;
                    next = searchWinnerFirstLvL(iconcomp,icon);
                    if(next==0) {
                        computer(iconcomp, icon);
                        searchWinnerFirstLvL(iconcomp, icon);
                    }
                }

            }
        });
        button8.addActionListener(new ActionListener( ) {
            public void actionPerformed(ActionEvent e) {

                if(b[8]==1) {
                    button8.setIcon(icon);
                    b[8]=0;
                    next = searchWinnerFirstLvL(iconcomp,icon);
                    if(next==0) {
                        computer(iconcomp, icon);
                        searchWinnerFirstLvL(iconcomp, icon);
                    }
                }

            }
        });
        button9.addActionListener(new ActionListener( ) {
            public void actionPerformed(ActionEvent e) {

                if(b[9]==1) {
                    button9.setIcon(icon);
                    b[9]=0;
                    next = searchWinnerFirstLvL(iconcomp,icon);
                    if(next==0) {
                        computer(iconcomp, icon);
                        searchWinnerFirstLvL(iconcomp, icon);
                    }
                }

            }
        });

    }                                                               // КІНЕЦЬ МЕТОДУ ХОДУ КОРИСТУВАЧА

    public static void computer(Icon iconComp, Icon icon) {                       // МЕТОД ХОДУ КОМП,ЮТЕРА
        if ( b[3]==1 && button1.getIcon() == button2.getIcon()&&button1.getIcon()!=null&&button2.getIcon()!=null&&(button1.getIcon()==iconComp && button2.getIcon()==iconComp)){
            button3.setIcon(iconComp);
            b[3]=0;
        }
        else if (b[3]==1 && button1.getIcon() == button2.getIcon()&&button1.getIcon()!=null&&button2.getIcon()!=null&&(button1.getIcon()==icon && button2.getIcon()==icon)){
            button3.setIcon(iconComp);
            b[3]=0;
        }
        else if ( b[1]==1 && button2.getIcon() == button3.getIcon()&&button2.getIcon()!=null&&button3.getIcon()!=null&&(button2.getIcon()==iconComp && button3.getIcon()==iconComp)){
            button1.setIcon(iconComp);
            b[1]=0;
        }
        else if (b[1]==1 &&button2.getIcon() == button3.getIcon()&&button2.getIcon()!=null&&button3.getIcon()!=null&&(button2.getIcon()==icon && button3.getIcon()==icon)){
            button1.setIcon(iconComp);
            b[1]=0;
        }
        else if ( b[2]==1 && button1.getIcon() == button3.getIcon()&&button1.getIcon()!=null&&button3.getIcon()!=null&&(button1.getIcon()==iconComp && button3.getIcon()==iconComp)){
            button2.setIcon(iconComp);
            b[2]=0;
        }
        else if (b[2]==1 &&button1.getIcon() == button3.getIcon()&&button1.getIcon()!=null&&button3.getIcon()!=null&&(button1.getIcon()==icon && button3.getIcon()==icon)){
            button2.setIcon(iconComp);
            b[2]=0;
        }
        //--------------------------------------------
        else if ( b[6]==1 && button4.getIcon() == button5.getIcon()&&button4.getIcon()!=null&&button5.getIcon()!=null&&(button4.getIcon()==iconComp && button5.getIcon()==iconComp)){
            button6.setIcon(iconComp);
            b[6]=0;
        }
        else if (b[6]==1 &&button4.getIcon() == button5.getIcon()&&button4.getIcon()!=null&&button5.getIcon()!=null&&(button4.getIcon()==icon && button5.getIcon()==icon)){
            button6.setIcon(iconComp);
            b[6]=0;
        }
        else if ( b[4]==1 && button5.getIcon() == button6.getIcon()&&button5.getIcon()!=null&&button6.getIcon()!=null&&(button6.getIcon()==iconComp && button5.getIcon()==iconComp)){
            button4.setIcon(iconComp);
            b[4]=0;
        }
        else if (b[4]==1 &&button5.getIcon() == button6.getIcon()&&button5.getIcon()!=null&&button6.getIcon()!=null&&(button6.getIcon()==icon && button5.getIcon()==icon)){
            button4.setIcon(iconComp);
            b[4]=0;
        }
        else if ( b[5]==1 && button4.getIcon() == button6.getIcon()&&button4.getIcon()!=null&&button6.getIcon()!=null&&(button4.getIcon()==iconComp && button6.getIcon()==iconComp)){
            button5.setIcon(iconComp);
            b[5]=0;
        }
        else if (b[5]==1 &&button4.getIcon() == button6.getIcon()&&button4.getIcon()!=null&&button6.getIcon()!=null&&(button4.getIcon()==icon && button6.getIcon()==icon)){
            button5.setIcon(iconComp);
            b[5]=0;
        }
        //---------------------------------------------
        else if ( b[9]==1 && button7.getIcon() == button8.getIcon()&&button7.getIcon()!=null&&button8.getIcon()!=null&&(button7.getIcon()==iconComp && button8.getIcon()==iconComp)){
            button9.setIcon(iconComp);
            b[9]=0;
        }
        else if (b[9]==1 &&button7.getIcon() == button8.getIcon()&&button7.getIcon()!=null&&button8.getIcon()!=null&&(button7.getIcon()==icon && button8.getIcon()==icon)){
            button9.setIcon(iconComp);
            b[9]=0;
        }
        else if ( b[8]==1 && button7.getIcon() == button9.getIcon()&&button7.getIcon()!=null&&button9.getIcon()!=null&&(button7.getIcon()==iconComp && button9.getIcon()==iconComp)){
            button8.setIcon(iconComp);
            b[8]=0;
        }
        else if (b[8]==1 &&button7.getIcon() == button9.getIcon()&&button7.getIcon()!=null&&button9.getIcon()!=null&&(button7.getIcon()==icon && button9.getIcon()==icon)){
            button8.setIcon(iconComp);
            b[8]=0;
        }
        else if ( b[7]==1 && button8.getIcon() == button9.getIcon()&&button8.getIcon()!=null&&button9.getIcon()!=null&&(button8.getIcon()==iconComp && button9.getIcon()==iconComp)){
            button7.setIcon(iconComp);
            b[7]=0;
        }
        else if (b[7]==1 &&button8.getIcon() == button9.getIcon()&&button8.getIcon()!=null&&button9.getIcon()!=null&&(button8.getIcon()==icon && button9.getIcon()==icon)){
            button7.setIcon(iconComp);
            b[7]=0;
        }
        //--------------------------------------------------
        else if ( b[7]==1 && button1.getIcon() == button4.getIcon()&&button1.getIcon()!=null&&button4.getIcon()!=null&&(button1.getIcon()==iconComp && button4.getIcon()==iconComp)){
            button7.setIcon(iconComp);
            b[7]=0;
        }
        else if (b[7]==1 &&button1.getIcon() == button4.getIcon()&&button1.getIcon()!=null&&button4.getIcon()!=null&&(button1.getIcon()==icon && button4.getIcon()==icon)){
            button7.setIcon(iconComp);
            b[7]=0;
        }
        else if ( b[4]==1 && button1.getIcon() == button7.getIcon()&&button1.getIcon()!=null&&button7.getIcon()!=null&&(button1.getIcon()==iconComp && button7.getIcon()==iconComp)){
            button4.setIcon(iconComp);
            b[4]=0;
        }
        else if (b[4]==1 && button1.getIcon() == button7.getIcon()&&button1.getIcon()!=null&&button7.getIcon()!=null&&(button1.getIcon()==icon && button7.getIcon()==icon)){
            button4.setIcon(iconComp);
            b[4]=0;
        }
        else if ( b[1]==1 && button4.getIcon() == button7.getIcon()&&button4.getIcon()!=null&&button7.getIcon()!=null&&(button4.getIcon()==iconComp && button7.getIcon()==iconComp)){
            button1.setIcon(iconComp);
            b[1]=0;
        }
        else if (b[1]==1 && button4.getIcon() == button7.getIcon()&&button4.getIcon()!=null&&button7.getIcon()!=null&&(button4.getIcon()==icon && button7.getIcon()==icon)){
            button1.setIcon(iconComp);
            b[1]=0;
        }
        //-------------------------------------------------
        else if  (b[8]==1 && button2.getIcon() == button5.getIcon()&&button2.getIcon()!=null&&button5.getIcon()!=null&&(button2.getIcon()==iconComp && button5.getIcon()==iconComp)){
            button8.setIcon(iconComp);
            b[8]=0;
        }
        else if (b[8]==1 && button2.getIcon() == button5.getIcon()&&button2.getIcon()!=null&&button5.getIcon()!=null&&(button2.getIcon()==icon && button5.getIcon()==icon)){
            button8.setIcon(iconComp);
            b[8]=0;
        }
        else if ( b[5]==1 && button2.getIcon() == button8.getIcon()&&button2.getIcon()!=null&&button8.getIcon()!=null&&(button8.getIcon()==iconComp && button2.getIcon()==iconComp)){
            button5.setIcon(iconComp);
            b[5]=0;
        }
        else if (b[5]==1 && button2.getIcon() == button8.getIcon()&&button2.getIcon()!=null&&button8.getIcon()!=null&&(button8.getIcon()==icon && button2.getIcon()==icon)){
            button5.setIcon(iconComp);
            b[5]=0;
        }
        else if ( b[2]==1 && button5.getIcon() == button8.getIcon()&&button5.getIcon()!=null&&button8.getIcon()!=null&&(button8.getIcon()==iconComp && button5.getIcon()==iconComp)){
            button2.setIcon(iconComp);
            b[2]=0;
        }
        else if ( b[2]==1 && button5.getIcon() == button8.getIcon()&&button5.getIcon()!=null&&button8.getIcon()!=null&&(button8.getIcon()==icon && button5.getIcon()==icon)){
            button2.setIcon(iconComp);
            b[2]=0;
        }
        //-------------------------------------------------
        else if ( b[9]==1 && button3.getIcon() == button6.getIcon()&&button3.getIcon()!=null&&button6.getIcon()!=null&&(button3.getIcon()==iconComp && button6.getIcon()==iconComp)){
            button9.setIcon(iconComp);
            b[9]=0;
        }
        else if (b[9]==1 &&button3.getIcon() == button6.getIcon()&&button3.getIcon()!=null&&button6.getIcon()!=null&&(button3.getIcon()==icon && button6.getIcon()==icon)){
            button9.setIcon(iconComp);
            b[9]=0;
        }
        else if ( b[6]==1 && button3.getIcon() == button9.getIcon()&&button3.getIcon()!=null&&button9.getIcon()!=null&&(button3.getIcon()==iconComp && button9.getIcon()==iconComp)){
            button6.setIcon(iconComp);
            b[6]=0;
        }
        else if (b[6]==1 &&button3.getIcon() == button9.getIcon()&&button3.getIcon()!=null&&button9.getIcon()!=null&&(button3.getIcon()==icon && button9.getIcon()==icon)){
            button6.setIcon(iconComp);
            b[6]=0;
        }
        else if ( b[3]==1 && button6.getIcon() == button9.getIcon()&&button6.getIcon()!=null&&button9.getIcon()!=null&&(button6.getIcon()==iconComp && button9.getIcon()==iconComp)){
            button3.setIcon(iconComp);
            b[3]=0;
        }
        else if (b[3]==1 &&button6.getIcon() == button9.getIcon()&&button6.getIcon()!=null&&button9.getIcon()!=null&&(button6.getIcon()==icon && button9.getIcon()==icon)){
            button3.setIcon(iconComp);
            b[3]=0;
        }
        //--------------------------------------------------
        else if ( b[9]==1 && button1.getIcon() == button5.getIcon()&&button1.getIcon()!=null&&button5.getIcon()!=null&&(button1.getIcon()==iconComp && button5.getIcon()==iconComp)){
            button9.setIcon(iconComp);
            b[9]=0;
        }
        else if (b[9]==1 && button1.getIcon() == button5.getIcon()&&button1.getIcon()!=null&&button5.getIcon()!=null&&(button1.getIcon()==icon && button5.getIcon()==icon)){
            button9.setIcon(iconComp);
            b[9]=0;
        }
        else if ( b[5]==1 && button1.getIcon() == button9.getIcon()&&button1.getIcon()!=null&&button9.getIcon()!=null&&(button1.getIcon()==iconComp && button9.getIcon()==iconComp)){
            button5.setIcon(iconComp);
            b[5]=0;
        }
        else if (b[5]==1 && button1.getIcon() == button9.getIcon()&&button1.getIcon()!=null&&button9.getIcon()!=null&&(button1.getIcon()==icon && button9.getIcon()==icon)){
            button5.setIcon(iconComp);
            b[5]=0;
        }
        else if (b[1]==1 && button5.getIcon() == button9.getIcon()&&button5.getIcon()!=null&&button9.getIcon()!=null&&(button5.getIcon()==iconComp && button9.getIcon()==iconComp)){
            button1.setIcon(iconComp);
            b[1]=0;
        }
        else if (b[1]==1 && button5.getIcon() == button9.getIcon()&&button5.getIcon()!=null&&button9.getIcon()!=null&&(button5.getIcon()==icon && button9.getIcon()==icon)){
            button1.setIcon(iconComp);
            b[1]=0;
        }
        //----------------------------------------------------
        else if (b[7]==1 && button3.getIcon() == button5.getIcon()&&button3.getIcon()!=null&&button5.getIcon()!=null&&(button3.getIcon()==iconComp && button5.getIcon()==iconComp)){
            button7.setIcon(iconComp);
            b[7]=0;
        }
        else if (b[7]==1 && button3.getIcon() == button5.getIcon()&&button3.getIcon()!=null&&button5.getIcon()!=null&&(button3.getIcon()==icon && button5.getIcon()==icon)){
            button7.setIcon(iconComp);
            b[7]=0;
        }
        else if (b[5]==1 && button3.getIcon() == button7.getIcon()&&button3.getIcon()!=null&&button7.getIcon()!=null&&(button3.getIcon()==iconComp && button7.getIcon()==iconComp)){
            button5.setIcon(iconComp);
            b[5]=0;
        }
        else if (b[5]==1 &&button3.getIcon() == button7.getIcon()&&button3.getIcon()!=null&&button7.getIcon()!=null&&(button3.getIcon()==icon && button7.getIcon()==icon)){
            button5.setIcon(iconComp);
            b[5]=0;
        }
        else if (b[3]==1 && button5.getIcon() == button7.getIcon()&&button5.getIcon()!=null&&button7.getIcon()!=null&&(button5.getIcon()==iconComp && button7.getIcon()==iconComp)){
            button3.setIcon(iconComp);
            b[3]=0;
        }
        else if (b[3]==1 &&button5.getIcon() == button7.getIcon()&&button5.getIcon()!=null&&button7.getIcon()!=null&&(button5.getIcon()==icon && button7.getIcon()==icon)){
            button3.setIcon(iconComp);
            b[3]=0;
        }
        else {
            if (button1.getIcon()!=iconComp && b[1]==1){
                button1.setIcon(iconComp);
                b[1]=0;
            }
            else if (button8.getIcon()!=iconComp && b[8]==1){
                button8.setIcon(iconComp);
                b[8]=0;
            }
            else if (button6.getIcon()!=iconComp && b[6]==1){
                button6.setIcon(iconComp);
                b[6]=0;
            }
            else if(button2.getIcon()!=iconComp && b[2]==1){
                button2.setIcon(iconComp);
                b[2]=0;
            }
            else if (button5.getIcon()!=iconComp && b[5]==1){
                button5.setIcon(iconComp);
                b[5]=0;
            }
            else if (button3.getIcon()!=iconComp && b[3]==1){
                button3.setIcon(iconComp);
                b[3]=0;
            }
            else if (button9.getIcon()==iconComp && b[9]==1) {
                button9.setIcon(iconComp);
                b[9] = 0;
            }
            else if (button4.getIcon()!=iconComp && b[4]==1){
                button4.setIcon(iconComp);
                b[4]=0;
            }
            else if (button7.getIcon()!=iconComp && b[7]==1){
                button7.setIcon(iconComp);
                b[7]=0;
            }
        }
    }                                                                           // КІНЕЦЬ МЕТОДУ ХОДУ КОМП,ЮТЕРА


    public static int searchWinnerSecondsLvL(Icon iconComp, Icon icon) {             // Метод перевірки хто переміг
        ShowWinner showWinner = new ShowWinner();
        //----------------------------------------------------
        if (button1.getIcon() ==icon && button2.getIcon() == icon &&button3.getIcon()==icon){
            winner="Player";
            showWinner.showWin(winner);
            return 1;
        }
        else if (button1.getIcon() ==iconComp && button2.getIcon() == iconComp &&button3.getIcon()==iconComp){
            winner="Computer";
            showWinner.showWin(winner);
            return 1;
        }
        //----------------------------------------------------
        if (button4.getIcon() == icon && button5.getIcon() == icon &&button6.getIcon()==icon){
            winner="Player";
            showWinner.showWin(winner);
            return 1;
        }
        else if (button4.getIcon() == iconComp && button5.getIcon() == iconComp &&button6.getIcon()==iconComp){
            winner="Computer";
            showWinner.showWin(winner);
            return 1;
        }
        //----------------------------------------------------
        if (button7.getIcon() == icon && button8.getIcon() == icon &&button9.getIcon()==icon){
            winner="Player";
            showWinner.showWin(winner);
            return 1;
        }
        else if (button7.getIcon() == iconComp && button8.getIcon() == iconComp &&button9.getIcon()==iconComp){
            winner="Computer";
            showWinner.showWin(winner);
            return 1;
        }
        //----------------------------------------------------
        if (button1.getIcon() == icon && button4.getIcon() == icon && button7.getIcon()==icon){
            winner="Player";
            showWinner.showWin(winner);
            return 1;
        }
        else if (button1.getIcon() == iconComp && button4.getIcon() == iconComp && button7.getIcon()==iconComp){
            winner="Computer";
            showWinner.showWin(winner);
            return 1;
        }
        //----------------------------------------------------
        if  (button3.getIcon() == icon && button6.getIcon()==icon && button9.getIcon()==icon){
            winner="Player";
            showWinner.showWin(winner);
            return 1;
        }
        else if  (button3.getIcon() == iconComp && button6.getIcon()==iconComp && button9.getIcon()==iconComp){
            winner="Computer";
            showWinner.showWin(winner);
            return 1;
        }
        //----------------------------------------------------
        if (button2.getIcon() == icon && button5.getIcon() == icon && button8.getIcon()==icon){
            winner="Player";
            showWinner.showWin(winner);
            return 1;
        }
        else if (button2.getIcon() == iconComp && button5.getIcon() == iconComp && button8.getIcon()==iconComp){
            winner="Computer";
            showWinner.showWin(winner);
            return 1;
        }
        //----------------------------------------------------
        if (button1.getIcon() == icon && button5.getIcon() == icon && button9.getIcon()==icon){
            winner="Player";
            showWinner.showWin(winner);
            return 1;
        }
        else if (button1.getIcon() == iconComp && button5.getIcon() == iconComp && button9.getIcon()==iconComp){
            winner="Computer";
            showWinner.showWin(winner);
            return 1;
        }
        //----------------------------------------------------
        if (button3.getIcon() == icon && button5.getIcon()== icon && button7.getIcon()==icon){
            winner="Player";
            showWinner.showWin(winner);
            return 1;
        }
        else if (button3.getIcon() == iconComp && button5.getIcon()== iconComp && button7.getIcon()==iconComp){
            winner="Computer";
            showWinner.showWin(winner);
            return 1;
        }
    return 0;}                                                 // Кінець методу який перевіряє хто переміг

    public static void main(String[] args) {
        useChoice();
    }      // ГОЛОВНИЙ МЕТОД (ЗАПУСКАЄ МЕДОТ ВИБОРУ)
}

